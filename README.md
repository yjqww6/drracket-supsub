# drracket-supsub

A DrRacket plugin to transform characters to super/subscript form.

* `c:m:,`, `esc;c:,` : transform selected characters (if no selected, the one before cursor) to superscript form. If there is no corresponding superscript form supported for any character selected, do nothing.
* `c​:m:​.`, `esc;c:.` : as above, but to subscript form.
